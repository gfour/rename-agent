package org.clyze.renameagent;

import java.io.File;
import java.io.IOException;
import java.lang.instrument.Instrumentation;
import java.nio.file.Files;
import java.security.AccessController;
import java.security.PrivilegedAction;

class PreMain {
    public static void premain(String agentArgs, Instrumentation inst) {
        if (agentArgs == null) {
            print("Please set target file to copy and target directory, using syntax '-javaagent:...=filename:target-dir'");
            return;
        }

        // Parse arguments (second argument is optional).
        int colonIndex = agentArgs.indexOf(':');
        String original, targetDir;
        if (colonIndex == -1) {
            original = agentArgs;
            targetDir = null;
        } else {
            original = agentArgs.substring(0, colonIndex);
            targetDir = agentArgs.substring(colonIndex + 1);
        }

        Runtime.getRuntime().addShutdownHook(new Thread() { 
                public void run() {
                    AccessController.doPrivileged(new PrivilegedAction<Object>() {
                            public Object run() {
                                print("Shutdown hook activated!");
                                File f = new File(original);
                                if (!f.exists()) {
                                    print("ERROR: file does not exist: " + f.toString());
                                    return null;
                                }
                                String name = f.getName();
                                int lastDot = name.lastIndexOf('.');
                                String suffix = "." + System.currentTimeMillis();
                                String newName = lastDot == -1 ? name + suffix :
                                    name.substring(0, lastDot) + suffix + "." + name.substring(lastDot + 1);
                                File newDir = new File(targetDir == null ? f.getParent() : targetDir);
                                File newFile =  new File(newDir, newName);

                                try {
                                    if (!newDir.exists())
                                        print("Creating " + newDir + ": " + newDir.mkdirs());
                                    Files.copy(f.toPath(), newFile.toPath());
                                    print("Copied: " + f + " -> " + newFile);
                                } catch (IOException ex) {
                                    print("Failed to copy: " + f + " -> " + newFile);
                                    ex.printStackTrace();
                                }
                                return null;
                            }});
                }}); 
        print("Shutdown hook added.");
    }

    private static void print(String s) {
        System.out.println("[rename-agent] " + s);
    }
}
