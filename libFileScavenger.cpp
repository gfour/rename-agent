#include <jvmti.h>
#include <chrono>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <string>
#include <cstring>

/*
 * A quick and dirty agent that scavenges a given file (copies it to
 * another directory with unique name).
 */

#define AGENT_ID "[file-scavenger-agent]"

using namespace std;

static jvmtiEnv *jvmti = NULL;
static jvmtiEventCallbacks callbacks;
static string timestamp = "00";
static string original;
static string targetDir;

static ostream& ts() {
    cout << AGENT_ID << " (" << timestamp << ") ";
    return cout;
}

inline bool exists(const string& name) {
    ifstream f(name.c_str());
    return f.good();
}

void JNICALL
ClassFileLoadHook(jvmtiEnv *jvmti_env,
                  JNIEnv* jni_env,
                  jclass class_being_redefined,
                  jobject loader,
                  const char* name,
                  jobject protection_domain,
                  jint class_data_len,
                  const unsigned char* class_data,
                  jint* new_class_data_len,
                  unsigned char** new_class_data) {
    string n(name);
    if (n.find("Native") != string::npos) {
        ts() << "Loading class: " << name << endl;
    }
}

/*
 * Initialize the agent: all setup happens here (including determining
 * the timestamp to use for the unique name).
 */
static jint Agent_Initialize(JavaVM *jvm, char *options, void *reserved) {
  int rc;

  if ((rc = jvm->GetEnv((void **)&jvmti, JVMTI_VERSION_1_2)) != JNI_OK) {
    cerr << "Unable to create jvmtiEnv, GetEnv failed, error = " << rc << endl;
    return JNI_ERR;
  }

  (void) memset(&callbacks, 0, sizeof(callbacks));
  callbacks.ClassFileLoadHook = &ClassFileLoadHook;
  if ((rc = jvmti->SetEventCallbacks(&callbacks, sizeof(callbacks))) != JNI_OK) {
    cerr << "SetEventCallbacks failed, error = " << rc << endl;
    return JNI_ERR;
  }

  string s = options;
  auto colonIndex = s.find(':');
  if (colonIndex == string::npos) {
      original = s;
      targetDir = ".";
  } else {
      original = s.substr(0, colonIndex);
      targetDir = s.substr(colonIndex + 1);
  }

  auto now = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
  timestamp = to_string(now);

  ts() << "Parameters: original = " << original << ", targetDir = " << targetDir << endl;

  return JNI_OK;
}

JNIEXPORT jint JNICALL Agent_OnLoad(JavaVM *jvm, char *options, void *reserved) {
    jint r = Agent_Initialize(jvm, options, reserved);
    ts() << "Agent loaded." << endl;
    return r;
}

JNIEXPORT jint JNICALL Agent_OnAttach(JavaVM *jvm, char *options, void *reserved) {
    jint r = Agent_Initialize(jvm, options, reserved);;
    ts() << "Agent attached." << endl;
    return r;
}

static void runCommand(string cmd) {
    ts() << "Command: " << cmd << endl;
    system(cmd.c_str());
}

/*
 * Activate the agent on unload (assumed to happen on JVM shutdown):
 * the target file is copied to its new destination, with a timestamp
 * added.
 */
JNIEXPORT void JNICALL
Agent_OnUnload(JavaVM *vm) {
    ts() << "Scavenging file: " << original << endl;

    if (!exists(targetDir)) {
        runCommand("mkdir -p " + targetDir);
        ts() << "Created directory: " << targetDir << endl;
    }

    auto lastDot = original.find('.');
    string suffix("." + timestamp);
    string newName;
    if (lastDot == string::npos)
        newName = original + suffix;
    else
        newName = original.substr(0, lastDot) + suffix + original.substr(lastDot);

    runCommand("cp " + original + " " + targetDir + "/" + newName);

    ts() << "Agent stopped." << endl;
}
