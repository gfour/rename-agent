AGENT_NAME=libFileScavenger
JDK_INCLUDE=/usr/lib/jvm/java-1.8.0-openjdk/include

all: agent

agent: $(AGENT_NAME).cpp
	g++ -g -shared -fPIC -Wall -o $(AGENT_NAME).so -I $(JDK_INCLUDE) -I $(JDK_INCLUDE)/linux $(AGENT_NAME).cpp

clean:
	rm -f $(AGENT_NAME).so $(AGENT_NAME).o
