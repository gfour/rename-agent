This agent renames a given file on JVM shutdown, using a timestamp for
the resulting file name. This functionality is provided both as a Java
agent and as a JVMTI agent.

## Build (Java agent) ##

To build the Java agent, run the following command:

```
./gradlew jar
```

To use the agent, pass the following option to the JVM:
`-javaagent:/path/to/rename-agent-1.0.jar=X.txt:dir`. On JVM shutdown,
X.txt will be renamed to dir/X.Y.txt, where Y is the current timestamp
(in milliseconds). If the "dir" parameter is missing, then the
directory of X.txt will be used.

To avoid some security manager restrictions, the original file may be
preserved (effectively doing a copy instead of a rename).

## Build (JVMTI agent) ##

To build the JVMTI agent, edit the JDK_INCLUDE variable in the
Makefile and run `make`. To use the agent, pass the following option
to the JVM:
`-agentpath:/path/to/libFileScavenger.so=X.txt:dir`. Parameters are
the same as those for the case of the Java agent. This agent is only
supported on POSIX systems (needs commands `cp`/`mkdir`).

## Example ##

To capture unique heap snapshots when running forked JUnit tests when
using Ant, place this in build.xml:

```
<junit ... fork="true">
  <jvmarg value="-agentpath:/path/to/libFileScavenger.so=heap.hprof:../hprofs" />
  <jvmarg value="-agentlib:hprof=heap=dump,format=b,depth=8,file=heap.hprof" />
```
